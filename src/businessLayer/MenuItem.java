package businessLayer;

import java.io.Serializable;

public abstract class MenuItem implements Serializable {

	private static final long serialVersionUID = 1L;
	private String name;
	private String ingredients;
	private float price;

	public MenuItem(String name, String ingredients, float price) {
		super();
		this.name = name;
		this.ingredients = ingredients;
		this.price = price;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getIngredients() {
		return ingredients;
	}

	public void setIngredients(String ingredients) {
		this.ingredients = ingredients;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

	public void add(MenuItem item) {
		throw new UnsupportedOperationException();
	}
	
	/**
	 * Abstract method that will be overwritten by subclasses
	 * @return price of a MenuItem
	 */
	public abstract float computePrice();

}
