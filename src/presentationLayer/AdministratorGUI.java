package presentationLayer;

import java.awt.CardLayout;
import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.SystemColor;
import java.util.ArrayList;
import java.util.List;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.RowSorter;
import javax.swing.SortOrder;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

import java.awt.Dimension;

public class AdministratorGUI extends JFrame {

	private AdministratorController controller = new AdministratorController(this);
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	JTextField editName;
	JTextField editPrice;
	JTextField editIngredients;
	JTable table;
	DefaultTableModel viewModel;
	JLayeredPane layeredPane;
	JPanel newPanel, editPanel, deletePanel;
	JScrollPane viewPanel;
	JTextField nameText;
	JTextField priceText;
	JTextField ingredientText;
	JButton newButton;
	JButton editButton;
	JButton deleteButton;
	JButton editItemButton;
	JButton viewButton;
	JButton helpButton;
	JButton addItemButton;
	JButton deleteItemButton;
	JRadioButton baseButton;
	JRadioButton compButton;
	JComboBox<String> deleteBox;
	JComboBox<String> editBox;
	SortedComboBoxModel<String> deleteModel;
	SortedComboBoxModel<String> editModel;

	public AdministratorGUI() {

		setTitle("Administrator");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 500, 350);
		contentPane = new JPanel();
		contentPane.setBackground(SystemColor.inactiveCaption);
		contentPane.setBorder(new EmptyBorder(10, 10, 10, 10));
		setContentPane(contentPane);
		GridBagLayout gbl_contentPane = new GridBagLayout();
		gbl_contentPane.columnWidths = new int[] { 158, 157, 78 };
		gbl_contentPane.rowHeights = new int[] { 14, 0, 0, 24, 157, 0 };
		gbl_contentPane.columnWeights = new double[] { 0.0, 0.0, 1.0 };
		gbl_contentPane.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE };
		contentPane.setLayout(gbl_contentPane);

		JLabel adminLabel = new JLabel("Administrator UI");
		GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
		gbc_lblNewLabel.insets = new Insets(0, 0, 5, 0);
		gbc_lblNewLabel.gridwidth = 3;
		gbc_lblNewLabel.gridx = 0;
		gbc_lblNewLabel.gridy = 0;
		contentPane.add(adminLabel, gbc_lblNewLabel);

		newButton = new JButton("New Menu Item");
		newButton.addActionListener(controller);
		GridBagConstraints gbc_newButton = new GridBagConstraints();
		gbc_newButton.fill = GridBagConstraints.HORIZONTAL;
		gbc_newButton.insets = new Insets(0, 0, 5, 5);
		gbc_newButton.gridx = 0;
		gbc_newButton.gridy = 1;
		contentPane.add(newButton, gbc_newButton);

		editButton = new JButton("Edit Menu Item");
		editButton.addActionListener(controller);
		GridBagConstraints gbc_editButton = new GridBagConstraints();
		gbc_editButton.fill = GridBagConstraints.HORIZONTAL;
		gbc_editButton.insets = new Insets(0, 0, 5, 5);
		gbc_editButton.gridx = 1;
		gbc_editButton.gridy = 1;
		contentPane.add(editButton, gbc_editButton);

		deleteButton = new JButton("Delete Menu Item");
		deleteButton.addActionListener(controller);
		GridBagConstraints gbc_deleteButton = new GridBagConstraints();
		gbc_deleteButton.fill = GridBagConstraints.HORIZONTAL;
		gbc_deleteButton.insets = new Insets(0, 0, 5, 0);
		gbc_deleteButton.gridx = 2;
		gbc_deleteButton.gridy = 1;
		contentPane.add(deleteButton, gbc_deleteButton);

		viewButton = new JButton("View All Menu Items");
		viewButton.addActionListener(controller);
		GridBagConstraints gbc_viewButton = new GridBagConstraints();
		gbc_viewButton.insets = new Insets(0, 0, 5, 0);
		gbc_viewButton.gridwidth = 3;
		gbc_viewButton.fill = GridBagConstraints.HORIZONTAL;
		gbc_viewButton.gridx = 0;
		gbc_viewButton.gridy = 2;
		contentPane.add(viewButton, gbc_viewButton);

		JSeparator separator = new JSeparator();
		separator.setBackground(Color.GRAY);
		separator.setForeground(Color.GRAY);
		GridBagConstraints gbc_separator = new GridBagConstraints();
		gbc_separator.gridwidth = 3;
		gbc_separator.fill = GridBagConstraints.HORIZONTAL;
		gbc_separator.insets = new Insets(0, 0, 5, 0);
		gbc_separator.gridx = 0;
		gbc_separator.gridy = 3;
		contentPane.add(separator, gbc_separator);

		layeredPane = new JLayeredPane();
		GridBagConstraints gbc_layeredPane = new GridBagConstraints();
		gbc_layeredPane.anchor = GridBagConstraints.NORTH;
		gbc_layeredPane.gridwidth = 3;
		gbc_layeredPane.fill = GridBagConstraints.HORIZONTAL;
		gbc_layeredPane.gridx = 0;
		gbc_layeredPane.gridy = 4;
		contentPane.add(layeredPane, gbc_layeredPane);
		layeredPane.setLayout(new CardLayout(0, 0));

		newPanel = new JPanel();
		newPanel.setBorder(new EmptyBorder(0, 0, 10, 0));
		newPanel.setBackground(SystemColor.inactiveCaption);
		layeredPane.add(newPanel);
		GridBagLayout gbl_newPanel = new GridBagLayout();
		gbl_newPanel.columnWidths = new int[] { 110, 288, 258, 0 };
		gbl_newPanel.rowHeights = new int[] { 25, 25, 0, 0, 20, 25, 25, 0 };
		gbl_newPanel.columnWeights = new double[] { 0.0, 0.0, 1.0, Double.MIN_VALUE };
		gbl_newPanel.rowWeights = new double[] { 0.0, 1.0, 0.0, 0.0, 0.0, 1.0, 0.0, Double.MIN_VALUE };
		newPanel.setLayout(gbl_newPanel);

		baseButton = new JRadioButton("Base product");
		baseButton.addActionListener(controller);
		baseButton.setSelected(true);
		baseButton.setBackground(SystemColor.inactiveCaption);
		buttonGroup.add(baseButton);
		GridBagConstraints gbc_rdbtnNewRadioButton = new GridBagConstraints();
		gbc_rdbtnNewRadioButton.fill = GridBagConstraints.HORIZONTAL;
		gbc_rdbtnNewRadioButton.insets = new Insets(0, 0, 5, 5);
		gbc_rdbtnNewRadioButton.gridx = 0;
		gbc_rdbtnNewRadioButton.gridy = 0;
		newPanel.add(baseButton, gbc_rdbtnNewRadioButton);

		compButton = new JRadioButton("Composite product");
		compButton.addActionListener(controller);
		compButton.setBackground(SystemColor.inactiveCaption);
		buttonGroup.add(compButton);
		GridBagConstraints gbc_rdbtnNewRadioButton_1 = new GridBagConstraints();
		gbc_rdbtnNewRadioButton_1.fill = GridBagConstraints.HORIZONTAL;
		gbc_rdbtnNewRadioButton_1.insets = new Insets(0, 0, 5, 5);
		gbc_rdbtnNewRadioButton_1.gridx = 1;
		gbc_rdbtnNewRadioButton_1.gridy = 0;
		newPanel.add(compButton, gbc_rdbtnNewRadioButton_1);

		helpButton = new JButton("?");
		helpButton.addActionListener(controller);
		GridBagConstraints gbc_helpButton = new GridBagConstraints();
		gbc_helpButton.anchor = GridBagConstraints.EAST;
		gbc_helpButton.gridwidth = 2;
		gbc_helpButton.insets = new Insets(0, 0, 5, 0);
		gbc_helpButton.gridx = 2;
		gbc_helpButton.gridy = 0;
		newPanel.add(helpButton, gbc_helpButton);

		JLabel nameLabel = new JLabel("Name");
		nameLabel.setBorder(new EmptyBorder(0, 7, 0, 0));
		GridBagConstraints gbc_lblNewLabel_1 = new GridBagConstraints();
		gbc_lblNewLabel_1.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabel_1.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_1.gridx = 0;
		gbc_lblNewLabel_1.gridy = 2;
		newPanel.add(nameLabel, gbc_lblNewLabel_1);

		nameText = new JTextField();
		GridBagConstraints gbc_textField_1 = new GridBagConstraints();
		gbc_textField_1.gridwidth = 2;
		gbc_textField_1.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField_1.insets = new Insets(0, 0, 5, 0);
		gbc_textField_1.gridx = 1;
		gbc_textField_1.gridy = 2;
		newPanel.add(nameText, gbc_textField_1);
		nameText.setColumns(10);

		JLabel priceLabel = new JLabel("Price");
		priceLabel.setBorder(new EmptyBorder(0, 7, 0, 0));
		GridBagConstraints gbc_lblNewLabel_2 = new GridBagConstraints();
		gbc_lblNewLabel_2.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabel_2.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_2.gridx = 0;
		gbc_lblNewLabel_2.gridy = 3;
		newPanel.add(priceLabel, gbc_lblNewLabel_2);

		priceText = new JTextField();
		priceText.setColumns(10);
		GridBagConstraints gbc_textField = new GridBagConstraints();
		gbc_textField.gridwidth = 2;
		gbc_textField.insets = new Insets(0, 0, 5, 0);
		gbc_textField.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField.gridx = 1;
		gbc_textField.gridy = 3;
		newPanel.add(priceText, gbc_textField);

		JLabel ingredientsLabel = new JLabel("Ingredients");
		ingredientsLabel.setBorder(new EmptyBorder(0, 7, 0, 0));
		GridBagConstraints gbc_lblNewLabel_3 = new GridBagConstraints();
		gbc_lblNewLabel_3.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabel_3.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_3.gridx = 0;
		gbc_lblNewLabel_3.gridy = 4;
		newPanel.add(ingredientsLabel, gbc_lblNewLabel_3);

		ingredientText = new JTextField();
		GridBagConstraints gbc_textField_2 = new GridBagConstraints();
		gbc_textField_2.gridwidth = 2;
		gbc_textField_2.insets = new Insets(0, 0, 5, 0);
		gbc_textField_2.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField_2.gridx = 1;
		gbc_textField_2.gridy = 4;
		newPanel.add(ingredientText, gbc_textField_2);
		ingredientText.setEnabled(false);
		ingredientText.setColumns(10);

		addItemButton = new JButton("Add item to menu");
		addItemButton.addActionListener(controller);
		GridBagConstraints gbc_btnNewButton_4 = new GridBagConstraints();
		gbc_btnNewButton_4.fill = GridBagConstraints.BOTH;
		gbc_btnNewButton_4.gridwidth = 3;
		gbc_btnNewButton_4.gridx = 0;
		gbc_btnNewButton_4.gridy = 6;
		newPanel.add(addItemButton, gbc_btnNewButton_4);

		editPanel = new JPanel();
		editPanel.setBorder(new EmptyBorder(0, 0, 10, 0));
		editPanel.setBackground(SystemColor.inactiveCaption);
		layeredPane.add(editPanel);
		GridBagLayout gbl_editPanel = new GridBagLayout();
		gbl_editPanel.columnWidths = new int[] { 110, 0, 0 };
		gbl_editPanel.rowHeights = new int[] { 30, 25, 0, 0, 0, 25, 0, 0 };
		gbl_editPanel.columnWeights = new double[] { 0.0, 1.0, Double.MIN_VALUE };
		gbl_editPanel.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE };
		editPanel.setLayout(gbl_editPanel);

		JLabel lblNewLabel_5 = new JLabel("Select item to edit: ");
		GridBagConstraints gbc_lblNewLabel_5 = new GridBagConstraints();
		gbc_lblNewLabel_5.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_5.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabel_5.gridx = 0;
		gbc_lblNewLabel_5.gridy = 0;
		editPanel.add(lblNewLabel_5, gbc_lblNewLabel_5);

		editModel = new SortedComboBoxModel<String>();
		editBox = new JComboBox<String>(editModel);
		editBox.addItemListener(controller);
		editBox.addActionListener(controller);
		editBox.setEditable(true);
		GridBagConstraints gbc_comboBox = new GridBagConstraints();
		gbc_comboBox.insets = new Insets(0, 0, 5, 0);
		gbc_comboBox.fill = GridBagConstraints.BOTH;
		gbc_comboBox.gridx = 1;
		gbc_comboBox.gridy = 0;
		editPanel.add(editBox, gbc_comboBox);

		JLabel lblNewLabel_7 = new JLabel("");
		GridBagConstraints gbc_lblNewLabel_7 = new GridBagConstraints();
		gbc_lblNewLabel_7.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_7.gridx = 0;
		gbc_lblNewLabel_7.gridy = 1;
		editPanel.add(lblNewLabel_7, gbc_lblNewLabel_7);

		JLabel lblNewLabel_6 = new JLabel("Name");
		lblNewLabel_6.setBorder(new EmptyBorder(0, 7, 0, 0));
		GridBagConstraints gbc_lblNewLabel_6 = new GridBagConstraints();
		gbc_lblNewLabel_6.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabel_6.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_6.gridx = 0;
		gbc_lblNewLabel_6.gridy = 2;
		editPanel.add(lblNewLabel_6, gbc_lblNewLabel_6);

		editName = new JTextField();
		GridBagConstraints gbc_textField_3 = new GridBagConstraints();
		gbc_textField_3.insets = new Insets(0, 0, 5, 0);
		gbc_textField_3.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField_3.gridx = 1;
		gbc_textField_3.gridy = 2;
		editPanel.add(editName, gbc_textField_3);
		editName.setColumns(10);

		JLabel lblNewLabel_9 = new JLabel("Price");
		lblNewLabel_9.setBorder(new EmptyBorder(0, 7, 0, 0));
		GridBagConstraints gbc_lblNewLabel_9 = new GridBagConstraints();
		gbc_lblNewLabel_9.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_9.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabel_9.gridx = 0;
		gbc_lblNewLabel_9.gridy = 3;
		editPanel.add(lblNewLabel_9, gbc_lblNewLabel_9);

		editPrice = new JTextField();
		GridBagConstraints gbc_textField_4 = new GridBagConstraints();
		gbc_textField_4.insets = new Insets(0, 0, 5, 0);
		gbc_textField_4.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField_4.gridx = 1;
		gbc_textField_4.gridy = 3;
		editPanel.add(editPrice, gbc_textField_4);
		editPrice.setColumns(10);

		JLabel lblNewLabel_8 = new JLabel("Ingredients");
		lblNewLabel_8.setBorder(new EmptyBorder(0, 7, 0, 0));
		GridBagConstraints gbc_lblNewLabel_8 = new GridBagConstraints();
		gbc_lblNewLabel_8.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabel_8.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_8.gridx = 0;
		gbc_lblNewLabel_8.gridy = 4;
		editPanel.add(lblNewLabel_8, gbc_lblNewLabel_8);

		editIngredients = new JTextField();
		GridBagConstraints gbc_textField_5 = new GridBagConstraints();
		gbc_textField_5.insets = new Insets(0, 0, 5, 0);
		gbc_textField_5.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField_5.gridx = 1;
		gbc_textField_5.gridy = 4;
		editPanel.add(editIngredients, gbc_textField_5);
		editIngredients.setColumns(10);

		JLabel lblNewLabel_10 = new JLabel("");
		GridBagConstraints gbc_lblNewLabel_10 = new GridBagConstraints();
		gbc_lblNewLabel_10.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_10.gridx = 0;
		gbc_lblNewLabel_10.gridy = 5;
		editPanel.add(lblNewLabel_10, gbc_lblNewLabel_10);

		editItemButton = new JButton("Edit selected item");
		editItemButton.addActionListener(controller);
		GridBagConstraints gbc_btnNewButton_5 = new GridBagConstraints();
		gbc_btnNewButton_5.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnNewButton_5.gridwidth = 2;
		gbc_btnNewButton_5.gridx = 0;
		gbc_btnNewButton_5.gridy = 6;
		editPanel.add(editItemButton, gbc_btnNewButton_5);

		deletePanel = new JPanel();
		deletePanel.setBackground(SystemColor.inactiveCaption);
		layeredPane.add(deletePanel);
		GridBagLayout gbl_deletePanel = new GridBagLayout();
		gbl_deletePanel.columnWidths = new int[] { 0, 0, 0, 0, 0 };
		gbl_deletePanel.rowHeights = new int[] { 0, 35, 0, 0 };
		gbl_deletePanel.columnWeights = new double[] { 0.0, 0.0, 1.0, 0.0, Double.MIN_VALUE };
		gbl_deletePanel.rowWeights = new double[] { 0.0, 0.0, 0.0, Double.MIN_VALUE };
		deletePanel.setLayout(gbl_deletePanel);

		JLabel lblNewLabel_11 = new JLabel("Select item to delete: ");
		GridBagConstraints gbc_lblNewLabel_11 = new GridBagConstraints();
		gbc_lblNewLabel_11.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_11.anchor = GridBagConstraints.EAST;
		gbc_lblNewLabel_11.gridx = 1;
		gbc_lblNewLabel_11.gridy = 0;
		deletePanel.add(lblNewLabel_11, gbc_lblNewLabel_11);

		deleteModel = new SortedComboBoxModel<String>();
		deleteBox = new JComboBox<String>(deleteModel);
		deleteBox.setEditable(true);
		GridBagConstraints gbc_comboBox_1 = new GridBagConstraints();
		gbc_comboBox_1.insets = new Insets(0, 0, 5, 5);
		gbc_comboBox_1.fill = GridBagConstraints.HORIZONTAL;
		gbc_comboBox_1.gridx = 2;
		gbc_comboBox_1.gridy = 0;
		deletePanel.add(deleteBox, gbc_comboBox_1);

		deleteItemButton = new JButton("Delete selected item");
		deleteItemButton.addActionListener(controller);
		GridBagConstraints gbc_btnNewButton_6 = new GridBagConstraints();
		gbc_btnNewButton_6.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnNewButton_6.gridwidth = 2;
		gbc_btnNewButton_6.insets = new Insets(0, 0, 0, 5);
		gbc_btnNewButton_6.gridx = 1;
		gbc_btnNewButton_6.gridy = 2;
		deletePanel.add(deleteItemButton, gbc_btnNewButton_6);

		viewPanel = new JScrollPane();
		viewPanel.setPreferredSize(new Dimension(150, 150));

		Object[] headers = { "Name", "Ingredients", "Price" };
		viewModel = new DefaultTableModel(headers, 0);
		table = new JTable(viewModel);
		TableRowSorter<TableModel> sorter = new TableRowSorter<TableModel>(table.getModel());
		List<RowSorter.SortKey> sortKeys = new ArrayList<>(25);
		sortKeys.add(new RowSorter.SortKey(0, SortOrder.ASCENDING));
		sortKeys.add(new RowSorter.SortKey(1, SortOrder.ASCENDING));
		sortKeys.add(new RowSorter.SortKey(2, SortOrder.ASCENDING));
		sorter.setSortKeys(sortKeys);
		table.setRowSorter(sorter);
		viewPanel.setOpaque(false);
		layeredPane.add(viewPanel);
		table.getColumnModel().getColumn(0).setPreferredWidth(110);
		table.getColumnModel().getColumn(1).setPreferredWidth(110);
		table.getColumnModel().getColumn(2).setPreferredWidth(40);
		viewPanel.setViewportView(table);
	}
}
