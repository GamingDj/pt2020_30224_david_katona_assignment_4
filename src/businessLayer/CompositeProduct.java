package businessLayer;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.StringTokenizer;

/**
 * Product consisting of more than one ingredient. For example: A salad is made
 * of lettuce, tomato, cucumber, etc...
 * 
 * @author David
 *
 */
public class CompositeProduct extends MenuItem implements Serializable {

	private static final long serialVersionUID = 1L;
	private ArrayList<MenuItem> subProducts = new ArrayList<MenuItem>();

	public CompositeProduct(String name, String ingredients, float price) {
		super(name, ingredients, price);
	}

	@Override
	public float computePrice() {
		return this.getPrice();
	}

	@Override
	public void add(MenuItem product) {
		this.subProducts.add(product);
	}

	@Override
	public String toString() {
		String s = "";
		s += this.getName() + ": " + this.getPrice() + "\n";
		for (MenuItem i : subProducts) {
			s += " -> " + i.getName() + ": " + i.computePrice() + "\n";
		}
		return s;
	}

	public void stringToArrayList(String ingredients) {
		subProducts.clear();
		StringTokenizer tok = new StringTokenizer(ingredients, ",");
		while (tok.hasMoreTokens()) {
			String name = tok.nextToken();
			if (name.startsWith(" "))
				name = name.substring(1);
			if (Character.isLowerCase(name.charAt(0))) {
				name = Character.toUpperCase(name.charAt(0)) + name.substring(1);
			}
			MenuItem ingred = new BaseProduct(name, "", 0);
			subProducts.add(ingred);
		}
	}

	public ArrayList<MenuItem> getSubProducts() {
		return (ArrayList<MenuItem>) subProducts;
	}

}
