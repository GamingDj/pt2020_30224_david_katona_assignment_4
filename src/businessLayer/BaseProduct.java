package businessLayer;

import java.io.Serializable;

/**
 * Simple product, one ingredient only. For example: Water, potato, beef, etc...
 * 
 * @author David
 *
 */
public class BaseProduct extends MenuItem implements Serializable {

	private static final long serialVersionUID = 1L;

	public BaseProduct(String name, String ingredients, float price) {
		super(name, ingredients, price);
	}

	@Override
	public float computePrice() {
		return getPrice();
	}

	@Override
	public String toString() {
		return this.getName() + ": " + computePrice();
	}

}
