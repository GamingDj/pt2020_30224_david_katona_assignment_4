package presentationLayer;

import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.SystemColor;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.RowSorter;
import javax.swing.SortOrder;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

public class WaiterGUI extends JFrame {

	private static final long serialVersionUID = 1L;
	private WaiterController controller = new WaiterController(this);
	JPanel contentPane, createPanel, billPanel;
	JScrollPane viewPanel;
	JLayeredPane layeredPane;
	JTable table;
	DefaultTableModel viewModel;
	JTextField tableTextField;
	JTextField itemsTextField;
	JTextField idTextField;
	JButton viewButton;
	JButton billButton;
	JButton createOrderButton;
	JButton orderButton;
	JButton clearButton;
	JButton generateBillButton;

	public WaiterGUI() {
		this.setTitle("Waiter");
		setBackground(SystemColor.inactiveCaption);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 500, 350);
		contentPane = new JPanel();
		contentPane.setBackground(SystemColor.inactiveCaption);
		contentPane.setBorder(new EmptyBorder(10, 10, 10, 10));
		setContentPane(contentPane);
		GridBagLayout gbl_contentPane = new GridBagLayout();
		gbl_contentPane.columnWidths = new int[] { 163, 138, 143, 0 };
		gbl_contentPane.rowHeights = new int[] { 30, 10, 18, 29, 0, 0 };
		gbl_contentPane.columnWeights = new double[] { 0.0, 1.0, 0.0, Double.MIN_VALUE };
		gbl_contentPane.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE };
		contentPane.setLayout(gbl_contentPane);

		JLabel waiteUILabel = new JLabel("Waiter UI");
		GridBagConstraints gbc_waiteUILabel = new GridBagConstraints();
		gbc_waiteUILabel.insets = new Insets(0, 0, 5, 5);
		gbc_waiteUILabel.gridx = 1;
		gbc_waiteUILabel.gridy = 0;
		contentPane.add(waiteUILabel, gbc_waiteUILabel);

		orderButton = new JButton("Create order");
		orderButton.addActionListener(controller);
		GridBagConstraints gbc_orderButton = new GridBagConstraints();
		gbc_orderButton.fill = GridBagConstraints.HORIZONTAL;
		gbc_orderButton.insets = new Insets(0, 0, 5, 5);
		gbc_orderButton.gridx = 0;
		gbc_orderButton.gridy = 1;
		contentPane.add(orderButton, gbc_orderButton);

		viewButton = new JButton("View all orders");
		viewButton.addActionListener(controller);
		GridBagConstraints gbc_viewButton = new GridBagConstraints();
		gbc_viewButton.fill = GridBagConstraints.HORIZONTAL;
		gbc_viewButton.insets = new Insets(0, 0, 5, 5);
		gbc_viewButton.gridx = 1;
		gbc_viewButton.gridy = 1;
		contentPane.add(viewButton, gbc_viewButton);

		billButton = new JButton("Create bill");
		billButton.addActionListener(controller);
		GridBagConstraints gbc_billButton = new GridBagConstraints();
		gbc_billButton.fill = GridBagConstraints.HORIZONTAL;
		gbc_billButton.anchor = GridBagConstraints.NORTH;
		gbc_billButton.insets = new Insets(0, 0, 5, 0);
		gbc_billButton.gridx = 2;
		gbc_billButton.gridy = 1;
		contentPane.add(billButton, gbc_billButton);

		clearButton = new JButton("Clear all orders");
		clearButton.setVisible(false);
		clearButton.addActionListener(controller);
		GridBagConstraints gbc_clearButton = new GridBagConstraints();
		gbc_clearButton.fill = GridBagConstraints.HORIZONTAL;
		gbc_clearButton.insets = new Insets(0, 0, 5, 5);
		gbc_clearButton.gridx = 1;
		gbc_clearButton.gridy = 2;
		contentPane.add(clearButton, gbc_clearButton);

		JSeparator separator = new JSeparator();
		separator.setBackground(Color.GRAY);
		GridBagConstraints gbc_separator = new GridBagConstraints();
		gbc_separator.gridwidth = 3;
		gbc_separator.fill = GridBagConstraints.HORIZONTAL;
		gbc_separator.insets = new Insets(0, 0, 5, 0);
		gbc_separator.gridx = 0;
		gbc_separator.gridy = 3;
		contentPane.add(separator, gbc_separator);

		layeredPane = new JLayeredPane();
		GridBagConstraints gbc_layeredPane = new GridBagConstraints();
		gbc_layeredPane.gridwidth = 3;
		gbc_layeredPane.fill = GridBagConstraints.BOTH;
		gbc_layeredPane.gridx = 0;
		gbc_layeredPane.gridy = 4;
		contentPane.add(layeredPane, gbc_layeredPane);
		layeredPane.setLayout(new CardLayout(0, 0));

		createPanel = new JPanel();
		createPanel.setBackground(SystemColor.inactiveCaption);
		layeredPane.add(createPanel);
		GridBagLayout gbl_createPanel = new GridBagLayout();
		gbl_createPanel.columnWidths = new int[] { 65, 0, 0 };
		gbl_createPanel.rowHeights = new int[] { 18, 0, 0, 29, 0, 0 };
		gbl_createPanel.columnWeights = new double[] { 0.0, 1.0, Double.MIN_VALUE };
		gbl_createPanel.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE };
		createPanel.setLayout(gbl_createPanel);

		JLabel tableLabel = new JLabel("Table");
		GridBagConstraints gbc_lblNewLabel_1 = new GridBagConstraints();
		gbc_lblNewLabel_1.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_1.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabel_1.gridx = 0;
		gbc_lblNewLabel_1.gridy = 1;
		createPanel.add(tableLabel, gbc_lblNewLabel_1);

		tableTextField = new JTextField();
		GridBagConstraints gbc_tableTextField = new GridBagConstraints();
		gbc_tableTextField.insets = new Insets(0, 0, 5, 0);
		gbc_tableTextField.fill = GridBagConstraints.HORIZONTAL;
		gbc_tableTextField.gridx = 1;
		gbc_tableTextField.gridy = 1;
		createPanel.add(tableTextField, gbc_tableTextField);
		tableTextField.setColumns(10);

		JLabel itemsLabel = new JLabel("Items");
		GridBagConstraints gbc_lblNewLabel_2 = new GridBagConstraints();
		gbc_lblNewLabel_2.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabel_2.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_2.gridx = 0;
		gbc_lblNewLabel_2.gridy = 2;
		createPanel.add(itemsLabel, gbc_lblNewLabel_2);

		itemsTextField = new JTextField();
		GridBagConstraints gbc_itemsTextField = new GridBagConstraints();
		gbc_itemsTextField.insets = new Insets(0, 0, 5, 0);
		gbc_itemsTextField.fill = GridBagConstraints.HORIZONTAL;
		gbc_itemsTextField.gridx = 1;
		gbc_itemsTextField.gridy = 2;
		createPanel.add(itemsTextField, gbc_itemsTextField);
		itemsTextField.setColumns(10);

		createOrderButton = new JButton("Create order");
		createOrderButton.addActionListener(controller);
		GridBagConstraints gbc_createOrderButton = new GridBagConstraints();
		gbc_createOrderButton.fill = GridBagConstraints.HORIZONTAL;
		gbc_createOrderButton.gridwidth = 2;
		gbc_createOrderButton.gridx = 0;
		gbc_createOrderButton.gridy = 4;
		createPanel.add(createOrderButton, gbc_createOrderButton);

		billPanel = new JPanel();
		billPanel.setBackground(SystemColor.inactiveCaption);
		layeredPane.add(billPanel);
		GridBagLayout gbl_billPanel = new GridBagLayout();
		gbl_billPanel.columnWidths = new int[] { 0, 0, 0 };
		gbl_billPanel.rowHeights = new int[] { 23, 0, 30, 0, 0 };
		gbl_billPanel.columnWeights = new double[] { 0.0, 1.0, Double.MIN_VALUE };
		gbl_billPanel.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE };
		billPanel.setLayout(gbl_billPanel);

		JLabel idLabel = new JLabel("Order ID: ");
		GridBagConstraints gbc_idLabel = new GridBagConstraints();
		gbc_idLabel.insets = new Insets(0, 0, 5, 5);
		gbc_idLabel.anchor = GridBagConstraints.EAST;
		gbc_idLabel.gridx = 0;
		gbc_idLabel.gridy = 1;
		billPanel.add(idLabel, gbc_idLabel);

		idTextField = new JTextField();
		GridBagConstraints gbc_idTextField = new GridBagConstraints();
		gbc_idTextField.insets = new Insets(0, 0, 5, 0);
		gbc_idTextField.fill = GridBagConstraints.HORIZONTAL;
		gbc_idTextField.gridx = 1;
		gbc_idTextField.gridy = 1;
		billPanel.add(idTextField, gbc_idTextField);
		idTextField.setColumns(10);

		generateBillButton = new JButton("Generate bill");
		generateBillButton.addActionListener(controller);
		GridBagConstraints gbc_generateBillButton = new GridBagConstraints();
		gbc_generateBillButton.fill = GridBagConstraints.HORIZONTAL;
		gbc_generateBillButton.gridwidth = 2;
		gbc_generateBillButton.gridx = 0;
		gbc_generateBillButton.gridy = 3;
		billPanel.add(generateBillButton, gbc_generateBillButton);

		viewPanel = new JScrollPane();
		viewPanel.setPreferredSize(new Dimension(100, 100));

		Object[] headers = { "ID", "Table", "Date", "Items", "Total" };
		viewModel = new DefaultTableModel(headers, 0);
		table = new JTable(viewModel);
		TableRowSorter<TableModel> sorter = new TableRowSorter<TableModel>(table.getModel());
		List<RowSorter.SortKey> sortKeys = new ArrayList<>(25);
		sortKeys.add(new RowSorter.SortKey(2, SortOrder.ASCENDING));
		sortKeys.add(new RowSorter.SortKey(0, SortOrder.ASCENDING));
		sortKeys.add(new RowSorter.SortKey(1, SortOrder.ASCENDING));
		sortKeys.add(new RowSorter.SortKey(4, SortOrder.ASCENDING));
		sorter.setSortKeys(sortKeys);
		table.setRowSorter(sorter);
		viewPanel.setOpaque(false);
		layeredPane.add(viewPanel);
		table.setRowSelectionAllowed(false);
		table.getColumnModel().getColumn(0).setPreferredWidth(30);
		table.getColumnModel().getColumn(1).setPreferredWidth(30);
		table.getColumnModel().getColumn(2).setPreferredWidth(90);
		table.getColumnModel().getColumn(3).setPreferredWidth(130);
		table.getColumnModel().getColumn(4).setPreferredWidth(30);
		viewPanel.setViewportView(table);
	}
}
