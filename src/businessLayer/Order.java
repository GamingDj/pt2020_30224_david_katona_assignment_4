package businessLayer;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

public class Order implements Serializable {

	private static final long serialVersionUID = 1L;
	private int orderID;
	private Date date;
	private int table;

	public Order(int id, int table) {
		super();
		this.orderID = id;
		this.date = new Date();
		this.table = table;
	}

	@Override
	public int hashCode() {
		return Objects.hash(orderID, date, table);
	}

	public int getOrderID() {
		return orderID;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public int getTable() {
		return table;
	}

	public void setTable(int table) {
		this.table = table;
	}
}
