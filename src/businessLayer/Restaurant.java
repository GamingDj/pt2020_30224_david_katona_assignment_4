package businessLayer;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Observable;
import java.util.StringTokenizer;

import javax.swing.JOptionPane;

import dataLayer.BillFileWriter;
import dataLayer.RestaurantSerializator;
import presentationLayer.MainController;

public class Restaurant extends Observable implements IRestaurantProcessing, Serializable {

	private static final long serialVersionUID = 1L;
	public static Restaurant instance = null;
	/**
	 * Containts information about every order, and also the products in the order.
	 */
	private HashMap<Order, ArrayList<MenuItem>> orders = new HashMap<>();
	/**
	 * Contains the entire menu of the restaurant.
	 */
	private ArrayList<MenuItem> menu = new ArrayList<MenuItem>();

	private Restaurant() {

	}

	/**
	 * Method used to get instance of Restaurant. If it does not exist, it will
	 * create a new one.
	 * 
	 * @return The only instance of Restaurant
	 */
	public static Restaurant getInstance() {
		if (instance == null)
			instance = new Restaurant();
		return instance;
	}

	/**
	 * Method used to load Restaurant data from restaurant.ser
	 */
	public static void loadData() {
		instance = RestaurantSerializator.deserialize();
		instance.addObserver(MainController.getChefGUI());
	}

	/**
	 * Method used to save Restaurant data to restaurant.ser
	 */
	public static void saveData() {
		RestaurantSerializator.serialize(instance);
	}

	/**
	 * Method used to add a new MenuItem to the menu
	 */
	@Override
	public void createNewMenuItem(MenuItem newItem) {
		assert newItem != null;
		int sizeBefore = menu.size();
		for (Iterator<MenuItem> it = menu.iterator(); it.hasNext();) {
			MenuItem i = it.next();
			if (i.getName().equals(newItem.getName())) {
				JOptionPane.showMessageDialog(null, "Item already in menu.", "Error", JOptionPane.WARNING_MESSAGE);
				return;
			}
		}
		menu.add(newItem);
		assert menu.size() == sizeBefore + 1;
		assert menu.contains(newItem);
		assert isWellFormed();
		saveData();
	}

	/**
	 * Method used to delete an item from the menu
	 */
	@Override
	public void deleteMenuItem(MenuItem item) {
		assert item != null;
		int sizeBefore = menu.size();
		for (Iterator<MenuItem> it = menu.iterator(); it.hasNext();) {
			MenuItem i = it.next();
			if (i instanceof CompositeProduct) {
				ArrayList<MenuItem> sub = ((CompositeProduct) i).getSubProducts();
				for (Iterator<MenuItem> it2 = sub.iterator(); it2.hasNext();) {
					MenuItem j = it2.next();
					if (j.getName().equals(item.getName())) {
						it.remove();
					}
				}
			}
			if (i.getName().equals(item.getName())) {
				it.remove();
			}
		}
		assert menu.size() == sizeBefore - 1;
		assert !menu.contains(item);
		assert isWellFormed();
		saveData();

	}

	/**
	 * Method used to edit an item in the menu
	 */
	@Override
	public void editMenuItem(MenuItem item, String name, String ingredients, float price) {
		assert item != null;
		for (Iterator<MenuItem> it = menu.iterator(); it.hasNext();) {
			MenuItem i = it.next();
			if (i instanceof CompositeProduct) {
				ArrayList<MenuItem> sub = ((CompositeProduct) i).getSubProducts();
				for (Iterator<MenuItem> it2 = sub.iterator(); it2.hasNext();) {
					MenuItem j = it2.next();
					if (j.getName().equals(item.getName()) && j.getPrice() == item.getPrice()) {
						j.setName(name);
						j.setIngredients("");
						j.setPrice(0);
					}
				}
				if (i.getName().equals(item.getName()) && i.getPrice() == item.getPrice()) {
					i.setName(name);
					i.setIngredients(ingredients);
					((CompositeProduct) i).stringToArrayList(ingredients);
					i.setPrice(price);
					assert i.getName().contentEquals(name);
				}

			}
			if (i instanceof BaseProduct) {
				if (i.getName().equals(item.getName()) && i.getPrice() == item.getPrice()) {
					i.setName(name);
					i.setIngredients("");
					i.setPrice(price);
					assert i.getName().equals(name);
				}
			}
		}
		assert isWellFormed();
		saveData();
	}

	/**
	 * Method used to create a new Order based on the table and items
	 */
	@Override
	public void createNewOrder(int table, String items) {
		if (table < 0) {
			JOptionPane.showMessageDialog(null, "Can't have negative table!", "Error", JOptionPane.ERROR_MESSAGE);
			return;
		}
		assert table >= 0;
		if (items.isEmpty() || items.equals(" ")) {
			JOptionPane.showMessageDialog(null, "Items can't be empty!", "Error", JOptionPane.ERROR_MESSAGE);
			return;
		}
		assert !items.isEmpty();
		int sizeBefore = orders.size();
		Order o = new Order(orders.size() + 1, table);
		StringTokenizer tok = new StringTokenizer(items, ",");
		ArrayList<MenuItem> itemList = new ArrayList<MenuItem>();
		while (tok.hasMoreTokens()) {
			String item = tok.nextToken();
			if (item.startsWith(" "))
				item = item.substring(1);
			if (Character.isLowerCase(item.charAt(0)))
				item = Character.toUpperCase(item.charAt(0)) + item.substring(1);
			boolean found = false;
			for (Iterator<MenuItem> it = Restaurant.getInstance().menu.iterator(); it.hasNext();) {
				MenuItem i = it.next();
				if (i.getName().equals(item)) {
					found = true;
					itemList.add(i);
				}
			}
			if (found == false) {
				JOptionPane.showMessageDialog(null, "Menu item \"" + item + "\" does not exist!", "Error",
						JOptionPane.ERROR_MESSAGE);
				return;
			}

		}
		orders.put(o, itemList);
		setChanged();
		notifyObservers(o);
		assert orders.size() == sizeBefore + 1;
		assert isWellFormed();
		saveData();
	}

	/**
	 * Method used to calculate the price of an order
	 */
	@Override
	public float computeOrderPrice(Order o) {
		assert o != null;
		float price = 0;
		for (MenuItem i : (ArrayList<MenuItem>) orders.get(o)) {
			price += i.computePrice();
		}
		assert price > 0;
		return price;
	}

	/**
	 * Method used to generate a bill for a given order
	 */
	@Override
	public void generateBill(int orderID) {
		boolean found = false;
		if (orderID > orders.size()) {
			JOptionPane.showMessageDialog(null, "Order does not exist", "Error", JOptionPane.ERROR_MESSAGE);
			return;
		}
		assert orderID <= orders.size();
		for (Order o : orders.keySet()) {
			if (o.getOrderID() == orderID) {
				found = true;
				BillFileWriter.generateBill(o.getOrderID(), o.getDate(), orders.get(o));
			}
		}
		if (found == false) {
			JOptionPane.showMessageDialog(null, "Order does not exist", "Error", JOptionPane.ERROR_MESSAGE);
			return;
		}
		assert found == true;
	}

	/**
	 * Method used to clear the orders
	 */
	@Override
	public void clearOrders() {
		this.orders.clear();
		assert orders.size() == 0;
		saveData();
	}

	/**
	 * Used to check integrity and correctness of restaurant
	 * 
	 * @return true of correct, false otherwise
	 */
	public boolean isWellFormed() {
		boolean menuOk = true;
		boolean ordersOk = true;
		// Test menu
		for (Iterator<MenuItem> it = instance.getMenu().iterator(); it.hasNext();) {
			MenuItem i = it.next();
			if (i.getName().contentEquals("") || i.getPrice() < 0) {
				menuOk = false;
			} else {
				if (i instanceof BaseProduct) {
					if (!i.getIngredients().contentEquals("")) {
						menuOk = false;
					}

				}
				if (i instanceof CompositeProduct) {
					if (i.getIngredients().contentEquals("")) {
						menuOk = false;
					}
				}
			}
		}
		// Test orders
		for (HashMap.Entry<Order, ArrayList<MenuItem>> entry : instance.getOrders().entrySet()) {
			Order o = entry.getKey();
			ArrayList<MenuItem> list = entry.getValue();
			if (o.getTable() < 0) {
				ordersOk = false;
			}
			if (list.isEmpty()) {
				ordersOk = false;
			}
		}
		if (menuOk == true && ordersOk == true)
			return true;
		return false;
	}

	public HashMap<Order, ArrayList<MenuItem>> getOrders() {
		return orders;
	}

	public ArrayList<MenuItem> getMenu() {
		return menu;
	}

}
