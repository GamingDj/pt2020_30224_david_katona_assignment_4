package businessLayer;

public interface IRestaurantProcessing {

	/**
	 * @pre newItem!=null
	 * @post menu.size() == menu.size()@pre + 1
	 * @post menu.contains(newItem)==true
	 */
	public void createNewMenuItem(MenuItem newItem);

	/**
	 * @pre item!=null
	 * @post menu.size() == menu.size()@pre - 1
	 * @post menu.contains(item) == false
	 */
	public void deleteMenuItem(MenuItem item);

	/**
	 * @pre item!=null
	 */
	public void editMenuItem(MenuItem item, String name, String ingredients, float price);

	/**
	 * @pre table >= 0
	 * @pre items.isEmpty() == false
	 * @post orders.size() == orders.size()@pre + 1
	 */
	public void createNewOrder(int table, String items);

	/**
	 * @pre o!=null
	 * @post return > 0
	 */
	public float computeOrderPrice(Order o);

	/**
	 * @pre orderID<=orders.size()
	 */
	public void generateBill(int orderID);

	/**
	 * @post orders.size()==0
	 */
	public void clearOrders();
}
