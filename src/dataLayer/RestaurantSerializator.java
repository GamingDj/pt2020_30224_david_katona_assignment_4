package dataLayer;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import businessLayer.Restaurant;

public class RestaurantSerializator {

	private static String path;

	/**
	 * Method used to save Restaurant to restaurant.ser
	 * @param rest: The Restaurant
	 */
	public static void serialize(Restaurant rest) {
		try {
			FileOutputStream fileOutputStream = new FileOutputStream(path);
			ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
			objectOutputStream.writeObject(rest);
			objectOutputStream.flush();
			fileOutputStream.close();
			objectOutputStream.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	/**
	 * Method used to load Restaurant from restaurant.ser
	 * @return The Restaurant
	 */
	public static Restaurant deserialize() {
		Restaurant rest = null;
		try {
			FileInputStream fileInputStream = new FileInputStream(path);
			ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
			rest = (Restaurant) objectInputStream.readObject();
			objectInputStream.close();
			fileInputStream.close();
		} catch (ClassNotFoundException | IOException e) {
			e.printStackTrace();
		}
		return rest;
	}

	public static void setPath(String p) {
		path = p;
	}
}
