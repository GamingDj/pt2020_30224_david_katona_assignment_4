package presentationLayer;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

public class MainController implements ActionListener {

	private MainGUI mainGUI;
	private String aboutMsg = "Restaurant Management System\nMade by Katona David in 2020";
	private static ChefGUI chefGUI = new ChefGUI();

	public MainController(MainGUI mainGUI) {
		super();
		this.mainGUI = mainGUI;
	}

	@Override
	public void actionPerformed(ActionEvent e) {

		if (e.getSource() == mainGUI.adminButton) {
			AdministratorGUI frame = new AdministratorGUI();
			frame.setVisible(true);
		}
		if (e.getSource() == mainGUI.waiterButton) {
			WaiterGUI frame = new WaiterGUI();
			frame.setVisible(true);
		}
		if (e.getSource() == mainGUI.chefButton) {
			//chefGUI = new ChefGUI();
			chefGUI.setVisible(true);
		}
		if (e.getSource() == mainGUI.aboutButton) {
			JOptionPane.showMessageDialog(mainGUI.aboutButton, aboutMsg, "About", JOptionPane.PLAIN_MESSAGE);
		}

	}

	public static ChefGUI getChefGUI() {
		return chefGUI;
	}

}
