package dataLayer;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import businessLayer.MenuItem;

/**
 * Class used to create billX.txt, where X is the OrderID
 * @author David
 *
 */
public class BillFileWriter {

	static File file = null;
	static FileWriter fw = null;
	private static SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

	public static void generateBill(int orderID, Date date, ArrayList<MenuItem> items) {
		try {
			file = new File("bill" + orderID + ".txt");
			fw = new FileWriter(file);
			fw.write("Order ID: " + orderID + " | Date: " + formatter.format(date) + "\n\n");
			float subTotal = 0;
			for (MenuItem i : items) {
				fw.write(i.getName() + ": " + i.getPrice() + "$\n");
				subTotal += i.getPrice();
			}
			fw.write("---------------------------\n");
			fw.write("Subtotal: " + String.format("%.2f", subTotal) + "$");
			fw.flush();
			fw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
