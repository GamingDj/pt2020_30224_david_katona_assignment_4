package presentationLayer;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.Iterator;

import javax.swing.JOptionPane;

import businessLayer.BaseProduct;
import businessLayer.CompositeProduct;
import businessLayer.MenuItem;
import businessLayer.Restaurant;

public class AdministratorController implements ActionListener, ItemListener {

	private AdministratorGUI adminGUI;

	public AdministratorController(AdministratorGUI adminGUI) {

		super();
		this.adminGUI = adminGUI;
	}

	public void switchPanels(Component panel) {

		adminGUI.layeredPane.removeAll();
		adminGUI.layeredPane.add(panel);
		adminGUI.layeredPane.repaint();
		adminGUI.layeredPane.revalidate();
	}

	@Override
	public void actionPerformed(ActionEvent e) {

		if (e.getSource() == adminGUI.newButton) {
			switchPanels(adminGUI.newPanel);
		}
		if (e.getSource() == adminGUI.helpButton) {
			JOptionPane.showMessageDialog(null,
					"To add a composite product, select the corresponding radio button and enter ingredients.\nIngredients need to be separated by a , (comma).\n  ex. Ham, Cheese, Bacon",
					"About", JOptionPane.PLAIN_MESSAGE);
		}
		if (e.getSource() == adminGUI.editButton) {
			adminGUI.editBox.removeAllItems();
			for (MenuItem i : Restaurant.getInstance().getMenu()) {
				adminGUI.editBox.addItem(i.getName());
			}
			switchPanels(adminGUI.editPanel);
		}
		if (e.getSource() == adminGUI.deleteButton) {
			adminGUI.deleteBox.removeAllItems();
			for (MenuItem i : Restaurant.getInstance().getMenu()) {
				adminGUI.deleteBox.addItem(i.getName());
			}
			switchPanels(adminGUI.deletePanel);

		}
		if (e.getSource() == adminGUI.viewButton) {
			adminGUI.viewModel.setRowCount(0);
			for (Iterator<MenuItem> it = Restaurant.getInstance().getMenu().iterator(); it.hasNext();) {
				MenuItem i = it.next();
				String name = i.getName(), ingr = i.getIngredients(), price = Float.toString(i.getPrice());
				Object[] row = { name, ingr, price };
				adminGUI.viewModel.addRow(row);
			}
			switchPanels(adminGUI.viewPanel);
		}
		if (e.getSource() == adminGUI.baseButton) {
			adminGUI.ingredientText.setEnabled(false);
		}
		if (e.getSource() == adminGUI.compButton) {
			adminGUI.ingredientText.setEnabled(true);
		}
		if (e.getSource() == adminGUI.addItemButton) {
			MenuItem newItem = null;
			if (adminGUI.nameText.getText().equals("")) {
				JOptionPane.showMessageDialog(null, "Name can't be empty!", "Error", JOptionPane.ERROR_MESSAGE);
				return;
			}
			if (adminGUI.priceText.getText().equals("")) {
				JOptionPane.showMessageDialog(null, "Price can't be empty!", "Error", JOptionPane.ERROR_MESSAGE);
				return;
			}
			if (adminGUI.baseButton.isSelected()) {
				newItem = new BaseProduct(adminGUI.nameText.getText(), "",
						Float.parseFloat(adminGUI.priceText.getText()));
			}
			if (adminGUI.compButton.isSelected()) {
				if (adminGUI.ingredientText.getText().equals("")) {
					JOptionPane.showMessageDialog(null, "Ingredients can't be empty!", "Error",
							JOptionPane.ERROR_MESSAGE);
					return;
				}
				newItem = new CompositeProduct(adminGUI.nameText.getText(), adminGUI.ingredientText.getText(),
						Float.parseFloat(adminGUI.priceText.getText()));
				((CompositeProduct) newItem).stringToArrayList(adminGUI.ingredientText.getText());
			}
			Restaurant.getInstance().createNewMenuItem(newItem);
		}
		if (e.getSource() == adminGUI.deleteItemButton) {
			MenuItem item = null;
			String toDel = adminGUI.deleteBox.getSelectedItem().toString();
			for (Iterator<MenuItem> it = Restaurant.getInstance().getMenu().iterator(); it.hasNext();) {
				MenuItem i = it.next();
				if (i.getName().equals(toDel))
					item = i;
			}
			Restaurant.getInstance().deleteMenuItem(item);
			adminGUI.deleteBox.removeAllItems();
			for (Iterator<MenuItem> it = Restaurant.getInstance().getMenu().iterator(); it.hasNext();) {
				MenuItem i = it.next();
				adminGUI.deleteBox.addItem(i.getName());
			}
		}
		if (e.getSource() == adminGUI.editItemButton) {
			MenuItem item = null;
			String toEdit = adminGUI.deleteBox.getSelectedItem().toString();
			for (Iterator<MenuItem> it = Restaurant.getInstance().getMenu().iterator(); it.hasNext();) {
				MenuItem i = it.next();
				if (i.getName().equals(toEdit))
					item = i;
			}
			Restaurant.getInstance().editMenuItem(item, adminGUI.editName.getText(), adminGUI.editIngredients.getText(),
					Float.parseFloat(adminGUI.editPrice.getText()));
		}
	}

	@Override
	public void itemStateChanged(ItemEvent e) {

		if (e.getStateChange() == ItemEvent.SELECTED) {
			MenuItem item = null;
			String toShow = adminGUI.editBox.getSelectedItem().toString();
			for (Iterator<MenuItem> it = Restaurant.getInstance().getMenu().iterator(); it.hasNext();) {
				MenuItem i = it.next();
				if (i.getName().equals(toShow))
					item = i;
			}
			if (item instanceof BaseProduct)
				adminGUI.editIngredients.setEnabled(false);
			if (item instanceof CompositeProduct)
				adminGUI.editIngredients.setEnabled(true);
			adminGUI.editName.setText(item.getName());
			adminGUI.editPrice.setText(Float.toString(item.getPrice()));
			adminGUI.editIngredients.setText(item.getIngredients());
		}
	}
}
