package businessLayer;

import dataLayer.RestaurantSerializator;
import presentationLayer.MainGUI;

public class MainClass {

	public static void main(String[] args) {

		RestaurantSerializator.setPath(args[0]);
		Restaurant.loadData();
		MainGUI gui = new MainGUI();
		gui.startGUI();

	}

}
