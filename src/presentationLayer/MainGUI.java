package presentationLayer;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.SystemColor;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

public class MainGUI extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	MainController controller = new MainController(this);
	JButton adminButton;
	JButton waiterButton;
	JButton chefButton;
	JButton aboutButton;

	public MainGUI() {
		contentPane = new JPanel();
		contentPane.setBackground(SystemColor.inactiveCaption);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		GridBagLayout gbl_contentPane = new GridBagLayout();
		gbl_contentPane.columnWidths = new int[] { 153, 128, 0, 0 };
		gbl_contentPane.rowHeights = new int[] { 0, 47, 0, 0, 0, 67, 0, 0 };
		gbl_contentPane.columnWeights = new double[] { 0.0, 0.0, 1.0, Double.MIN_VALUE };
		gbl_contentPane.rowWeights = new double[] { 0.0, 0.0, 1.0, 1.0, 1.0, 0.0, 0.0, Double.MIN_VALUE };
		contentPane.setLayout(gbl_contentPane);

		JLabel roleLabel = new JLabel("Please select your role");
		GridBagConstraints gbc_roleLabel = new GridBagConstraints();
		gbc_roleLabel.fill = GridBagConstraints.HORIZONTAL;
		gbc_roleLabel.insets = new Insets(0, 0, 5, 5);
		gbc_roleLabel.gridx = 0;
		gbc_roleLabel.gridy = 0;
		contentPane.add(roleLabel, gbc_roleLabel);

		adminButton = new JButton("Administrator");
		adminButton.addActionListener(controller);
		GridBagConstraints gbc_adminButton = new GridBagConstraints();
		gbc_adminButton.gridwidth = 3;
		gbc_adminButton.fill = GridBagConstraints.BOTH;
		gbc_adminButton.insets = new Insets(0, 0, 5, 0);
		gbc_adminButton.gridx = 0;
		gbc_adminButton.gridy = 2;
		contentPane.add(adminButton, gbc_adminButton);

		waiterButton = new JButton("Waiter");
		waiterButton.addActionListener(controller);
		GridBagConstraints gbc_waiterButton = new GridBagConstraints();
		gbc_waiterButton.gridwidth = 3;
		gbc_waiterButton.fill = GridBagConstraints.BOTH;
		gbc_waiterButton.insets = new Insets(0, 0, 5, 0);
		gbc_waiterButton.gridx = 0;
		gbc_waiterButton.gridy = 3;
		contentPane.add(waiterButton, gbc_waiterButton);

		chefButton = new JButton("Chef");
		chefButton.addActionListener(controller);
		GridBagConstraints gbc_chefButton = new GridBagConstraints();
		gbc_chefButton.gridwidth = 3;
		gbc_chefButton.insets = new Insets(0, 0, 5, 0);
		gbc_chefButton.fill = GridBagConstraints.BOTH;
		gbc_chefButton.gridx = 0;
		gbc_chefButton.gridy = 4;
		contentPane.add(chefButton, gbc_chefButton);

		aboutButton = new JButton("About");
		aboutButton.addActionListener(controller);
		GridBagConstraints gbc_btnNewButton_3 = new GridBagConstraints();
		gbc_btnNewButton_3.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnNewButton_3.insets = new Insets(0, 0, 0, 5);
		gbc_btnNewButton_3.gridx = 0;
		gbc_btnNewButton_3.gridy = 6;
		contentPane.add(aboutButton, gbc_btnNewButton_3);
	}

	public void startGUI() {
		this.setTitle("Restaurant Management System");
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setBounds(100, 100, 450, 300);
		this.setVisible(true);
	}

}
