package presentationLayer;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;

import businessLayer.MenuItem;
import businessLayer.Order;
import businessLayer.Restaurant;

public class WaiterController implements ActionListener {

	private WaiterGUI waiterGUI;

	public WaiterController(WaiterGUI waiterGUI) {
		super();
		this.waiterGUI = waiterGUI;
	}

	public void switchPanels(Component panel) {
		waiterGUI.layeredPane.removeAll();
		waiterGUI.layeredPane.add(panel);
		waiterGUI.layeredPane.repaint();
		waiterGUI.layeredPane.revalidate();
	}

	public void actionPerformed(ActionEvent e) {

		if (e.getSource() == waiterGUI.billButton) {
			waiterGUI.clearButton.setVisible(false);
			switchPanels(waiterGUI.billPanel);
		}
		if (e.getSource() == waiterGUI.orderButton) {
			waiterGUI.clearButton.setVisible(false);
			switchPanels(waiterGUI.createPanel);
		}
		if (e.getSource() == waiterGUI.clearButton) {
			Restaurant.getInstance().clearOrders();
			waiterGUI.viewModel.setRowCount(0);
		}
		if (e.getSource() == waiterGUI.viewButton) {
			waiterGUI.clearButton.setVisible(true);
			waiterGUI.viewModel.setRowCount(0);
			for (HashMap.Entry<Order, ArrayList<MenuItem>> entry : Restaurant.getInstance().getOrders().entrySet()) {
				DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
				String id = Integer.toString(entry.getKey().getOrderID());
				String table = Integer.toString(entry.getKey().getTable());
				String date = df.format(entry.getKey().getDate());
				ArrayList<MenuItem> itemList = entry.getValue();
				String items = "";
				for (MenuItem i : itemList)
					items += i.getName() + ", ";
				items = items.substring(0, items.length()-2);
				String total = String.format("%.2f", Restaurant.getInstance().computeOrderPrice(entry.getKey()));
				Object[] row = { id, table, date, items, total };
				waiterGUI.viewModel.addRow(row);
			}
			switchPanels(waiterGUI.viewPanel);
		}
		if (e.getSource() == waiterGUI.createOrderButton) {
			Restaurant.getInstance().createNewOrder(Integer.parseInt(waiterGUI.tableTextField.getText()),
					waiterGUI.itemsTextField.getText());
		}
		if (e.getSource() == waiterGUI.generateBillButton) {
			Restaurant.getInstance().generateBill(Integer.parseInt(waiterGUI.idTextField.getText()));
		}
	}

}
